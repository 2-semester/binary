#include <iostream>
#include "arrays.cpp"
#include "binary.cpp"

using namespace std;

int main()
{
    int length = 5;
    double* array = WorkingWithArrays::allocateMemoryDouble(length);
    WorkingWithArrays::randomValues(array, length);
    WorkingWithArrays::displayArrayDouble(array, length);
    for (int i = 0; i < length; i++)
    {
        int len;
        int* binary = WorkingWithBinary::toBinary((int) array[i], len);
        cout << array[i] << " - ";
        WorkingWithArrays::displayArrayInt(binary, len);
        cout << endl;
    }
    WorkingWithArrays::bubbleSort(array, length, WorkingWithArrays::compareByUnits);
    WorkingWithArrays::displayArrayDouble(array, length);

    int numberOfUnits;
    cout << "Enter number of units: ";
    cin >> numberOfUnits;
    int numberOfZeros;
    cout << "Enter number of zeros: ";
    cin >> numberOfZeros;

    int newLength;
    array = WorkingWithArrays::deleteBy(array, length, newLength, numberOfUnits, numberOfZeros);
    WorkingWithArrays::displayArrayDouble(array, newLength);
}
