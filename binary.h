#pragma once
namespace WorkingWithBinary
{
	int* toBinary(int, int&);
	int countBy(int, int);
}
