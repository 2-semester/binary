#include "arrays.h"
#include "binary.h"
#include <iostream>

using namespace std;

int* WorkingWithArrays::allocateMemoryInt(int n)
{
    return new int[n];
}

double* WorkingWithArrays::allocateMemoryDouble(int n)
{
    return new double[n];
}

void WorkingWithArrays::displayArrayDouble(double* p, int n)
{
    for (double* q = p; q < p + n; q++)
    {
        cout << *q << " ";
    }
    cout << endl;
}

void WorkingWithArrays::displayArrayInt(int* p, int n)
{
    for (int* q = p; q < p + n; q++)
    {
        cout << *q;
    }
}

void WorkingWithArrays::randomValues(double* p, int n)
{
    srand(time(nullptr));    

    for (double* q = p; q < p + n; q++)
    {
        *q = (rand() % 10000) / 100.0;
    }
}

void WorkingWithArrays::bubbleSort(double* arr, int length, comparer comparer)
{
    for (int i = 0; i < length; i++)
    {
        for (int j = length - 1; j > i; j--)
        {
            if (comparer(arr[j], arr[j - 1]) < 0)
            {
                double tmp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = tmp;
            }
        }
    }
}

int WorkingWithArrays::compareByUnits(double firstNumber, double secondNumber)
{
    return WorkingWithBinary::countBy(firstNumber, 1) - WorkingWithBinary::countBy(secondNumber, 1);
}

double* WorkingWithArrays::deleteBy(double* p, int length, int& newLength, int numberOfUnits, int numberOfZeros)
{
    newLength = 0;

    double* array = allocateMemoryDouble(length);
    for (double* r = p, *q = array; r < p + length; r++)
    {
        if (WorkingWithBinary::countBy(*r, 1) != numberOfUnits || WorkingWithBinary::countBy(*r, 0) != numberOfZeros)
        {
            *q = *r;
            q++;
            newLength++;
        }
    }

    double* result = allocateMemoryDouble(newLength);
    for (int i = 0; i < newLength; i++)
    {
        result[i] = array[i];
    }

    delete []array;
    return result;
}