#pragma once
namespace WorkingWithArrays
{
    using comparer = int (*)(double, double);

    double* allocateMemoryDouble(int);
    void displayArrayDouble(double*, int);
    void randomValues(double*, int);
    int* allocateMemoryInt(int);
    void displayArrayInt(int*, int);
    void bubbleSort(double*, int, comparer);
	double* deleteBy(double*, int, int&, int, int);
    int compareByUnits(double, double);
}
