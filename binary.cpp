#include "arrays.h"
#include "binary.h"
#include <iostream>

int* WorkingWithBinary::toBinary(int number, int& length)
{
    length = 0;

    int* binaryNumber = WorkingWithArrays::allocateMemoryInt(127);
    for (int i = 0; number; i++)
    {
        binaryNumber[i] = number % 2;
        number /= 2;
        length++;
    }

    int* result = WorkingWithArrays::allocateMemoryInt(length);
    for (int i = 0; i < length; i++)
    {
        result[i] = binaryNumber[length - 1 - i];
    }

    delete []binaryNumber;
    return result;
}

int WorkingWithBinary::countBy(int number, int digit)
{
    int length = 0;
    int* binary = WorkingWithBinary::toBinary(number, length);

    int result = 0;

    for (int i = 0; i < length; i++)
    {
        if (binary[i] == digit)
        {
            result++;
        }
    }

    return result;
}
